package com.strekha.simplecalc

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.support.v4.widget.SlidingPaneLayout
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var calculableView: CalculableView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculableView = findViewById(R.id.calculable_view) as CalculableView

        initSlidingPanel()
        initFragments()
    }

    private fun initFragments() {
        val baseFragment: BaseButtonsFragments = fragmentManager.findFragmentById(R.id.base_fragment) as BaseButtonsFragments
        val extraFragment: ExtraButtonsFragment = fragmentManager.findFragmentById(R.id.extra_fragment) as ExtraButtonsFragment

        baseFragment.onClickListener = calculableView::addText
        baseFragment.onResultListener = calculableView::showResult
        baseFragment.onDeleteListener = calculableView::deleteText

        extraFragment.onClickListener = calculableView::addText

    }

    private fun initSlidingPanel() {
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) return

        val slidingPane = findViewById(R.id.sliding_pane) as SlidingPaneLayout
        slidingPane.openPane()
        slidingPane.sliderFadeColor = Color.TRANSPARENT
    }
}

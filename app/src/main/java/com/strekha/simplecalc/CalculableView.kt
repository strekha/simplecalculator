package com.strekha.simplecalc

import android.content.Context
import android.os.Parcelable
import android.util.AttributeSet
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import com.udojava.evalex.Expression
import java.util.*
import android.os.Parcel
import android.view.View
import java.math.BigDecimal


class CalculableView(context: Context, attr: AttributeSet) : FrameLayout(context, attr) {

    companion object {
        @JvmStatic
        private val EMPTY_STRING = ""

        @JvmStatic
        private val ERROR = "Error :("
    }

    private var inputEditText: EditText
    private var resultTextView: TextView
    private var queue: MutableList<CalcButton> = ArrayList()
    private var isResultShowing = false

    init {
        val view = inflate(context, R.layout.view_calculable, this)
        inputEditText = view.findViewById(R.id.input_edit_text)
        resultTextView = view.findViewById(R.id.result_text_view)
        inputEditText.showSoftInputOnFocus = false
    }

    fun addText(button: CalcButton) {
        if (isResultShowing) {
            if (button.isBinaryOperator) {
                parseToQueue(inputEditText.currentText)
            } else {
                inputEditText.setText(EMPTY_STRING)
            }
            isResultShowing = false
        }
        if (queue.isEmpty() && !button.isCanBeFirst) return
        if (!button.isRepeatable && queue.isNotEmpty() && getLast() == button) return


        var cursorPosition = inputEditText.selectionEnd
        var index = getIndexByCursorPosition(cursorPosition)
        if (queue.isNotEmpty()) index++
        if (!button.isRepeatable && queue.isNotEmpty() && !getLast().isRepeatable) {
            val newInput = inputEditText.currentText
                    .replaceLast(getLast().text, button.text, endPosition = cursorPosition)
            queue[index - 1] = button
            inputEditText.currentText = newInput
            inputEditText.setSelection(cursorPosition)
            showTempResult()
            return
        }
        if (button.isFunc && !queue.isEmpty() && !getLast().isBinaryOperator && !getLast().isFunc) {
            log(getLast())
            queue.add(index, CalcButton.MULTIPLY)
            inputEditText.currentText = getQueueText()
            cursorPosition++
            index++
        }
        queue.add(index, button)
        inputEditText.currentText = getQueueText()
        inputEditText.setSelection(cursorPosition + button.text.length)
        showTempResult()
    }

    fun deleteText() {
        if (isResultShowing) {
            inputEditText.setText(EMPTY_STRING)
            isResultShowing = false
        }
        if (inputEditText.selectionEnd == 0) return
        if (!queue.isEmpty()) {
            val cursorPosition = inputEditText.selectionEnd
            if (cursorPosition != inputEditText.currentText.length) {
                val index = getIndexByCursorPosition(cursorPosition)
                queue.removeAt(index)
                val textBeforeButton = queue.take(index)
                        .map { it.text }
                        .reduce { acc, s -> acc + s }
                val offset = cursorPosition - textBeforeButton.length
                inputEditText.setText(getQueueText())
                inputEditText.setSelection(cursorPosition - offset)
                showTempResult()
                return
            }
            inputEditText.removeLastSymbols(queue.last().text.length)
            queue.removeAt(queue.lastIndex)
            showTempResult()
        }
    }

    private fun getIndexByCursorPosition(position: Int): Int {
        var length = 0
        if (queue.isEmpty()) return 0
        for ((index, calcButton) in queue.withIndex()) {
            length += calcButton.text.length
            if (length >= position) {
                return index
            }
        }
        return queue.lastIndex
    }

    private fun showTempResult() {
        val result = getResult()
        if (result != ERROR) resultTextView.text = result
    }

    private fun getResult(): String {
        if (queue.isEmpty()) return EMPTY_STRING
        return try {
            val expression = queue.map { it.libText }.reduce { acc, s -> acc + s }
            val evalResult = Expression(expression).eval()
            if (evalResult >= BigDecimal.valueOf(10_000_000)) evalResult.toEngineeringString()
            else evalResult.toPlainString()
        } catch (ex: Exception) {
            ERROR
        }
    }

    private fun getLast() = queue[getIndexByCursorPosition(inputEditText.selectionEnd)]

    private fun getQueueText() = queue.map { it.text }.reduce { acc, s -> acc + s }

    fun showResult() {
        inputEditText.currentText = getResult()
        resultTextView.text = EMPTY_STRING
        queue.clear()
        isResultShowing = true
    }

    private fun parseToQueue(text: String) {
        val items = text.map { CalcButton.getByText(it.toString()) }
        queue.addAll(items)
    }

    override fun onSaveInstanceState(): Parcelable {
        val state = SavedState(super.onSaveInstanceState())
        state.isResultShowing = this.isResultShowing
        state.queue = this.queue
        return state
    }

    override fun onRestoreInstanceState(state: Parcelable?) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)
            isResultShowing = state.isResultShowing
            queue = state.queue
            showTempResult()
        } else super.onRestoreInstanceState(state)
    }

    internal class SavedState : View.BaseSavedState {

        var queue: MutableList<CalcButton> = ArrayList()
        var isResultShowing = false

        constructor(superState: Parcelable) : super(superState)

        private constructor(input: Parcel) : super(input) {
            this.isResultShowing = input.readInt() != 0
            val listONames = emptyList<String>()
            input.readList(listONames, MutableList::class.java.classLoader)
            queue = listONames.map { CalcButton.valueOf(it) }.toMutableList()
        }

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeInt(if (isResultShowing) 1 else 0)
            out.writeList(queue.map { it.toString() })
        }

        companion object {
            val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
                override fun createFromParcel(input: Parcel): SavedState {
                    return SavedState(input)
                }

                override fun newArray(size: Int): Array<SavedState?> {
                    return arrayOfNulls(size)
                }
            }
        }
    }
}
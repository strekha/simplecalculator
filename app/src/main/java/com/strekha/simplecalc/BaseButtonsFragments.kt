package com.strekha.simplecalc

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class BaseButtonsFragments : Fragment() {

    var onClickListener: ((CalcButton) -> Unit)? = null
    var onResultListener: (() -> Unit)? = null
    var onDeleteListener: (() -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.fragment_base, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        CalcButton.values().forEach { operator ->
            run {
                view?.findViewById<Button>(operator.id)?.setOnClickListener { onClickListener?.invoke(operator) }
            }
        }
        view?.findViewById<Button>(R.id.result_btn)?.setOnClickListener{ onResultListener?.invoke() }
        view?.findViewById<Button>(R.id.delete_btn)?.setOnClickListener{ onDeleteListener?.invoke() }
    }
}
package com.strekha.simplecalc

enum class CalcButton(
        val id: Int,
        val text: String,
        val libText: String = text.toUpperCase(),
        val isRepeatable: Boolean = true,
        val isCanBeFirst: Boolean = true,
        val isFunc: Boolean = false,
        val isBinaryOperator: Boolean = false) {

    ONE(R.id.one_btn, "1"),
    TWO(R.id.two_btn, "2"),
    THREE(R.id.three_btn, "3"),
    FOUR(R.id.four_btn, "4"),
    FIVE(R.id.five_btn, "5"),
    SIX(R.id.six_btn, "6"),
    SEVEN(R.id.seven_btn, "7"),
    EIGHT(R.id.eight_btn, "8"),
    NINE(R.id.nine_btn, "9"),
    ZERO(R.id.zero_btn, "0"),
    DOT(R.id.dot_btn, ",", ".", isRepeatable = false, isCanBeFirst = false),

    DIVIDE(R.id.divide_btn, "÷", "/", isRepeatable = false, isCanBeFirst = false, isBinaryOperator = true),
    MULTIPLY(R.id.multiply_btn, "×", "*", false, false, isBinaryOperator = true),
    MINUS(R.id.minus_btn, "-", isRepeatable = false, isCanBeFirst = true, isBinaryOperator = true),
    PLUS(R.id.plus_btn, "+", isRepeatable = false, isCanBeFirst = false, isBinaryOperator = true),

    SIN(R.id.sin_btn, "sin(", isFunc = true),
    COS(R.id.cos_btn, "cos(", isFunc = true),
    TAN(R.id.tan_btn, "tan(", isFunc = true),
    LN(R.id.ln_btn, "ln(", "LOG(", isFunc = true),
    LOG(R.id.log_btn, "log(", "LOG10(", isFunc = true),
    FACT(R.id.fact_btn, "!", isRepeatable =  false),
    PI(R.id.pi_btn, "π"),
    EXP(R.id.exp_btn, "e"),
    POW(R.id.pow_btn, "^", isRepeatable = false, isCanBeFirst = false),
    OPEN(R.id.open_bracket_btn, "("),
    CLOSE(R.id.close_bracket_btn, ")", isRepeatable = true, isCanBeFirst = false),
    SQRT(R.id.sqrt_btn, "√", isFunc = true),
    E(0, "E")
    ;

    companion object {
        fun getByText(text: String) = CalcButton.values().first { it.text == text || it.libText == text }
    }

}
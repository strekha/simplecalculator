package com.strekha.simplecalc

import android.util.Log
import android.widget.EditText

internal fun EditText.removeLastSymbols(count: Int) {
    val resultText = currentText.removeLast(count)
    setText(resultText)
    setSelection(resultText.length)
}

internal var EditText.currentText : String
    get() = text.toString()
    set(value) {
        setText(value)
        setSelection(value.length)
    }

internal fun String.removeLast(count: Int) : String {
    val lastIndex = this.length - count
    return substring(0, lastIndex)
}

internal fun String.replaceLast(old: String, new: String, endPosition: Int = this.length): String {
    val string = substring(0, endPosition)
    val end = if (endPosition < length) substring(endPosition, length)
              else ""
    return string.reversed().replaceFirst(old, new, true).reversed() + end
}

internal fun log(text: Any) = Log.d("log_tag", text.toString())
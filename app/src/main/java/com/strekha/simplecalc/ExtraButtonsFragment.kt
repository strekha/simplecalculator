package com.strekha.simplecalc

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

class ExtraButtonsFragment : Fragment() {

    var onClickListener: ((CalcButton) -> Unit)? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater!!.inflate(R.layout.fragment_extra, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        CalcButton.values().forEach { operator ->
            run {
                view?.findViewById<Button>(operator.id)?.setOnClickListener { onClickListener?.invoke(operator) }
            }
        }
    }
}